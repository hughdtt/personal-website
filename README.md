## Personal-Website
Static Website built with Gitlab Pages.

It's a simple and stylish page that highlights my skills and recent work. 

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

## Tools
- HTML/ CSS
- Bootstrap 4
- Transitions done with wow.js
    - Learn more about it here: https://wowjs.uk/
- Template/ Inspiration: https://colorlib.com/preview/#noah

## Blog
Find out about for motivations and inspirations for making this site.

